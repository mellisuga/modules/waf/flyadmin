
import FlyAuth from 'core/flyauth.ui/index.js';
import XHR from 'core/utils/xhr_async.js';

(async function() {
  try {
    let vmid = document.body.querySelector(".vert_mid");
    let flyauth_element = FlyAuth.login({
    }, XHR);
    vmid.appendChild(flyauth_element);
    flyauth_element.email_input.focus();

//    await FlyAuth.prepare(false, XHR);
  } catch (e) {
    console.error(e.stack);
  }
})();

/*
import XHR from 'core/utils/xhr_async.js';

var next_url = getParam("next_url");
if (next_url) {
  var next_url_input = document.createElement("input");
  next_url_input.type = "hidden";
  next_url_input.name = "next_url";
  next_url_input.value = next_url;

  var auth_form = document.getElementById("auth_form");
  auth_form.appendChild(next_url_input);
}

function getParam(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

let signin_form = document.getElementById("auth_form");
let err_msg_field = document.getElementById("err_msg_field");

signin_form.addEventListener('submit', async function(e) {
  e.preventDefault();
  try {
    let formData = new FormData(signin_form)
    let send_data = {};
    for (let [key, value] of formData.entries()) {
      send_data[key] = value;
    }
    console.log("XHR POST", send_data);
    const response = await XHR.post("/admin-auth.io", send_data);

    if (response.err) {
      err_msg_field.innerHTML = "Neteisingai įvestas e-pašto adresas arba slaptažodis!";
    } else if (response.next_url) {
      window.localStorage.setItem('access_token', response.access_token.data);
      window.location.replace(response.next_url);
    }
    console.log(response);
  } catch (e) {
    console.error(e.stack);
  }
});
*/
