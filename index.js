

import path from 'path';

import Auth from 'flyauth';


export default class FlyAdmin {
  static async construct(waf, cfg) {
    try {
      console.log("+ FLYADMIN");
      let _this = new FlyAdmin(waf);
      if (!cfg) cfg = {};

      let acfg = {
        table_name: "admin_accounts",
        auth_paths: {
          unauthorized: "/admin_auth",
          authenticated: cfg.authenticated
        },
        prefix: 'admin',
        rights: true
      }
        

      let auth = _this.auth = await Auth.init(waf.router.app, waf.mail_session, waf.aura, acfg);
      auth.builtin_pages(waf.pages, {});

      console.log("FLYATUH RDY");

      waf.pages.serve_dir("/admin_auth", path.resolve(new URL('.', import.meta.url).pathname, 'dist'), {
        name: "admin_auth",
        globals_path: waf.globals_path,
        dev_only: true,
        auth: auth
      });

//      await waf.router.listen(auth);
      waf.modules.init_controls(auth);
      
      waf.admin = _this;
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(waf) {

  }
}
